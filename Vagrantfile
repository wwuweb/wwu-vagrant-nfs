VAGRANTFILE_API_VERSION = '2'

Vagrant.require_version ">= 2.1.2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = 'wwu-vagrant-bionic'
  config.vm.box_url = 'https://bitbucket.org/wwuweb/vbox-ubuntu64/raw/master/vbox-bionic64.box'
  config.vm.box_download_checksum = '6e793a0df379c03a5d4231c657be1778'
  config.vm.box_download_checksum_type = 'md5'

  config.vm.provider 'virtualbox' do |vbox|
    vbox.cpus = 2
    vbox.memory = 4096
    vbox.customize ['modifyvm', :id, '--ioapic', 'on']
  end

  config.ssh.forward_agent = true

  config.vm.network 'forwarded_port', guest: 80, host: 8080
  config.vm.network 'private_network', type: 'dhcp'

  config.nfs.verify_installed = false

  config.vm.synced_folder '.', '/vagrant', type: 'nfs',
    mount_options: ['fsc','rsize=32768','wsize=32768','intr','noacl','noatime','nodiratime','nocto']

  if Vagrant.has_plugin?('vagrant-bindfs')
    config.bindfs.bind_folder '/vagrant', '/vagrant', :multithreaded => true
  end

  config.vm.provision 'shell', path: 'shell/puppet.sh'

  config.vm.provision 'puppet' do |puppet|
    puppet.environment = 'vagrant'
    puppet.environment_path = 'puppet/environments'
  end
end

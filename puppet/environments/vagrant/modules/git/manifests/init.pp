class git {
  package { 'git':
    ensure => present
  }

  Package['git'] -> Git::Repository <| |>
}
define git::branch (
  $ensure = present,
  $remote = 'origin'
) {
  validate_re($ensure, '^(present|absent)$',
  "${ensure} is not supported for ensure. Allowed values are 'present' or 'absent'")
  validate_re($title, '^.+:.+$',
  "${title} is not supported for branch name. Allowed values follow the format repository:branch")

  $parts = split($title, ':')
  $repository = $parts[0]
  $branch = $parts[1]

  if ! defined(Git::Repository[$repository]) {
    fail("Git repository ${repository} is not defined.")
  }

  $repository_ensure = getparam(Git::Repository[$repository], 'ensure')
  $repository_directory = getparam(Git::Repository[$repository], 'directory')
  $repository_owner = getparam(Git::Repository[$repository], 'owner')

  Exec {
    user => $repository_owner,
    cwd  => $repository_directory,
    path => [
      '/usr/local/bin',
      '/usr/local/sbin',
      '/usr/bin',
      '/usr/sbin',
      '/bin',
      '/sbin'
    ]
  }

  if $repository_ensure == 'present' {
    if $ensure == 'present' {
      exec { "git-checkout-${repository}:${branch}":
        command => "git checkout -B ${branch}",
        unless  => "git branch | grep '* ${branch}' >/dev/null 2>/dev/null",
        require => Exec["git-fetch-${repository}"]
      }
    } else {
      exec { "git-detete-branch-${repository}:${branch}":
        command => "git branch -D ${branch}",
        onlyif  => "git branch | grep '${branch}' >/dev/null 2>/dev/null",
        require => Exec["git-fetch-${repository}"]
      }
    }
  }
}

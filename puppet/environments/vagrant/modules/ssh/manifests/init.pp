class ssh (
  $install = true
) {
  package { 'openssh-client':
    ensure => present
  }
  
  $basedir_ensure = $install ? {
    true    => directory,
    default => absent
  }

  $concat_ensure = $install ? {
    true    => present,
    default => absent
  }

  file { $::ssh_basedir:
    ensure => $basedir_ensure,
    mode   => '0755'
  }

  concat { 'ssh_known_hosts':
    path            => '/etc/ssh/ssh_known_hosts',
    ensure          => $concat_ensure,
    force           => true,
    mode            => '0644',
    ensure_newline  => true,
    require         => Package['openssh-client']
  }

  File[$::ssh_basedir] -> Ssh::Known_host <| |>
}

Facter.add("ssh_basedir") do
  setcode do
    File.join(Puppet[:vardir], "ssh")
  end
end

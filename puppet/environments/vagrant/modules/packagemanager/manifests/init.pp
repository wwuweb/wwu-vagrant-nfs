class packagemanager (
  $provider = $packagemanager::params::provider
) inherits packagemanager::params {
  include "packagemanager::provider::${provider}"

  Package <| |> {
    require => Exec["update-${provider}-package-lists"]
  }
}

class packagemanager::params {
  $provider = $facts['os']['family'] ? {
    'RedHat'  => 'rpm',
    'Debian'  => 'apt',
    'Suse'    => 'yum',
    default   => fail('Operating system not supported')
  }
}

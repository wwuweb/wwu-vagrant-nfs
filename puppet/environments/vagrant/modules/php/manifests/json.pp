class php::json (
  $package = $php::params::json_package
) inherits php::params {
  package { $package:
    ensure  => present,
    require => Package['php']
  }
}
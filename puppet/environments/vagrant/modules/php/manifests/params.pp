class php::params {
  $package = $facts['os']['name'] ? {
    'Ubuntu' => $facts['os']['release']['major'] ? {
      '14.04' => 'php5',
      default => 'php',
    },
    default => 'php',
  }

  $config_root = $facts['os']['name'] ? {
    'Ubuntu' => $facts['os']['release']['major'] ? {
      '18.04' => '/etc/php/7.2',
      default => "/etc/${package}",
    },
    default => "/etc/${package}",
  }

  $curl_package = "${package}-curl"
  $gd_package = "${package}-gd"
  $json_package = "${package}-json"
  $ldap_package = "${package}-ldap"
  $mbstring_package = "${package}-mbstring"

  $mcrypt_package = $facts['os']['name'] ? {
    'Ubuntu' => $facts['os']['release']['major'] ? {
      '18.04' => 'none',
      default => "${package}-mcrypt",
    },
    default =>  "${package}-mcrypt",
  }

  $mysql_package = "${package}-mysql"
  $xdebug_package = "${package}-xdebug"
  $xml_package = "${package}-xml"
}

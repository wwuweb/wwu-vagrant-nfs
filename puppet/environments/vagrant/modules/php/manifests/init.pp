class php (
  $package     = $php::params::package,
  $config_root = $php::params::config_root
) inherits php::params {
  package { $package:
    ensure  => present,
    alias   => 'php'
  }

  file { "${config_root}/apache2/php.ini":
    ensure  => present,
    source  => 'puppet:///modules/php/php.ini',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    notify  => Service['apache'],
    require => Package['php']
  }

  file { "${config_root}/cli/php.ini":
    ensure  => present,
    source  => 'puppet:///modules/php/php.ini',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Package['php']
  }
}
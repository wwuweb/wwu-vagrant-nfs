class php::curl (
  $package = $php::params::curl_package
) inherits php::params {
  package { $package:
    ensure  => present,
    require => Package['php']
  }
}

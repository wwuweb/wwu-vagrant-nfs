class php::mcrypt (
  $package     = $php::params::mcrypt_package,
  $config_root = $php::params::config_root
) inherits php::params {
  if $package != 'none' {
    package { $package:
      ensure  => present,
      alias   => 'php-mcrypt',
      require => Package['php']
    }

    file { "${config_root}/apache2/conf.d/mcrypt.ini":
      ensure  => link,
      target  => "${config_root}/mods-available/mcrypt.ini",
      owner   => 'root',
      group   => 'root',
      notify  => Service['apache'],
      require => Package['php-mcrypt']
    }
  }
}
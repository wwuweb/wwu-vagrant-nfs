class php::gd (
  $package = $php::params::gd_package
) inherits php::params {
  package { $package:
    ensure  => present,
    require => Package['php']
  }
}

class drush::params {
  include composer::params

  $install_path = "${composer::params::composer_vendor_dir}/drush/drush"
  $config_path  = "/etc/drush"
}

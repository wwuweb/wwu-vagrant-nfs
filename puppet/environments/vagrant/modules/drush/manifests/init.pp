class drush (
  $install      = true,
  $version      = undef,
  $config_path  = $drush::params::config_path
) inherits drush::params {
  require php::xml

  Exec {
    environment => [
      "COMPOSER_HOME=${composer::params::composer_home}"
    ],
    path => [
      '/usr/local/bin',
      '/usr/local/sbin',
      '/usr/bin',
      '/usr/sbin',
      '/bin',
      '/sbin'
    ]
  }

  if $install == false {
    $ensure = absent
  } elsif $version {
    $ensure = $version
  } else {
    $ensure = present
  }

  package { 'drush/drush':
    ensure    => $ensure,
    provider  => 'composer'
  }

  file { $config_path:
    ensure  => directory,
    owner   => 'root',
    group   => 'root',
    mode    => '0755'
  }

  if $ensure != absent {
    exec { 'drush-composer-install':
      cwd         => $drush::params::install_path,
      command     => 'composer install',
      subscribe   => Package['drush/drush'],
      refreshonly => true
    }

    exec { 'drush':
      cwd         => $drush::params::install_path,
      command     => 'drush',
      subscribe   => Exec['drush-composer-install'],
      refreshonly => true
    }
  }
}

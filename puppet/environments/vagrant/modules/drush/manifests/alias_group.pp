define drush::alias_group (
  $ensure       = 'present',
  $alias_group  = $title,
  $path         = $drush::config_path,
  $owner        = 'root',
  $group        = 'root',
  $site_list    = undef
) {
  if !defined(Class['drush']) {
    fail('The class drush must be defined.')
  }

  $file_attributes = {
    ensure  => directory,
    owner   => $owner,
    group   => $group,
    mode    => '0755'
  }
  ensure_resource('file', $path, $file_attributes)

  if $site_list {
    concat::fragment { "${alias_group}-group":
      target  => "${alias_group}-group",
      content => template('drush/alias_list.erb')
    }

    $alias_path = "${path}/${alias_group}.alias.drushrc.php"
  } else {
    $alias_path = "${path}/${alias_group}.aliases.drushrc.php"
  }

  concat { "${alias_group}-group":
    path            => $alias_path,
    owner           => $owner,
    group           => $group,
    ensure          => $ensure,
    ensure_newline  => true
  }

  concat::fragment { "${alias_group}-group-header":
    target  => "${alias_group}-group",
    content => template('drush/alias_header.erb'),
    order   => '01'
  }
}

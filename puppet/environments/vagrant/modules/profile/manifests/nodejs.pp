class profile::nodejs {
  $repo_url_suffix = lookup('profile::nodejs::repo_url_suffix')
  $legacy_package  = lookup('profile::nodejs::legacy_package')

  class { '::nodejs':
    manage_package_repo => true,
    repo_pin            => '1000',
    repo_url_suffix     => $repo_url_suffix
  }

  if $legacy_package != 'none' {
    package { $legacy_package:
      ensure => present
    }
  }

  package { 'grunt-cli':
    ensure   => present,
    provider => 'npm'
  }

  package { 'gulp':
    ensure   => present,
    provider => 'npm'
  }
}

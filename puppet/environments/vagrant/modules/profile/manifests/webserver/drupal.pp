class profile::webserver::drupal inherits profile::webserver {
  $drush_version   = lookup('profile::webserver::drupal::drush_version')
  $apache_doc_root = lookup('profile::webserver::drupal::apache_doc_root')
  $virtual_host    = lookup('profile::webserver::drupal::virtual_host')
  $database_user   = lookup('profile::webserver::drupal::database_user')
  $database_pass   = lookup('profile::webserver::drupal::database_pass')

  include php::gd
  include php::curl
  include php::ldap
  include php::mbstring
  include php::mcrypt
  include php::xml

  class { '::composer':
    require => Class['php']
  }

  class { '::drush':
    version => $drush_version
  }

  apache::module { 'rewrite': }

  apache::vhost { $virtual_host:
    doc_root  => $apache_doc_root
  }

  mysql::user { $database_user:
    password  => $database_pass
  }

  package { 'drupal/drupal-extension':
    ensure   => present,
    provider => 'composer'
  }

  package { 'drupal/coder':
    ensure   => present,
    provider => 'composer',
  }

  package { 'squizlabs/php_codesniffer':
    ensure   => '2.7.0',
    provider => 'composer',
    require  => Package['drupal/coder']
  }

  file { '/usr/bin/composer/squizlabs/php_codesniffer/CodeSniffer/Standards/Drupal':
    target  => '/usr/bin/composer/drupal/coder/coder_sniffer/Drupal',
    ensure  => link,
    require => Package['squizlabs/php_codesniffer']
  }
}

class profile::sass {
  $compass_version   = lookup('profile::sass::compass_version')
  $sass_version      = lookup('profile::sass::sass_version')
  $zen_grids_version = lookup('profile::sass::zen_grids_version')
  $ruby_dev_package  = lookup('profile::sass::ruby_dev_package')

  package { $ruby_dev_package:
    ensure => present,
    alias  => 'ruby-dev'
  }

  package { 'bundler':
    ensure   => present,
    provider => 'gem'
  }

  package { 'compass':
    ensure   => $compass_version,
    provider => 'gem'
  }

  package { 'sass':
    ensure   => $sass_version,
    provider => 'gem'
  }

  package { 'zen-grids':
    ensure   => $zen_grids_version,
    provider => 'gem'
  }
}

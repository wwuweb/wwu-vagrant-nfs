define apache::module (
  $module = $title,
  $enable = true
) {
  if ! defined(Class['apache']) {
    fail('Class apache must be defined.')
  }

  Exec {
    path => [
      '/usr/local/bin',
      '/usr/local/sbin',
      '/usr/bin',
      '/usr/sbin',
      '/bin',
      '/sbin'
    ]
  }

  if $enable {
    exec { "${module}-enable":
      command => "a2enmod ${module}",
      onlyif  => "ls ${apache::server_root}/${apache::mods_available} | grep '${module}' >/dev/null 2>/dev/null",
      unless  => "ls ${apache::server_root}/${apache::mods_enabled} | grep '${module}' >/dev/null 2>/dev/null",
      notify  => Service['apache'],
      require => Package['apache']
    }
  } else {
    exec { "${module}-disable":
      command => "a2dismod ${module}",
      onlyif  => "ls ${apache::server_root}/${apache::mods_available} | grep '${module}' >/dev/null 2>/dev/null",
      notify  => Service['apache'],
      require => Package['apache']
    }
  }
}
class apache::params {
  $package           = 'apache2'
  $service           = 'apache2'
  $web_user          = 'www-data'
  $web_group         = 'www-data'
  $server_root       = '/etc/apache2'
  $vhosts_available  = 'sites-available'
  $vhosts_enabled    = 'sites-enabled'
  $mods_available    = 'mods-available'
  $mods_enabled      = 'mods-enabled'
}

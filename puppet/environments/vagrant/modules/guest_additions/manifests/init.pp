class guest_additions (
  $version      = undef,
  $cwd          = $guest_additions::params::cwd,
  $mount_point  = $guest_additions::params::mount_point,
  $install_dir  = $guest_additions::params::install_dir,
  $link_target  = $guest_additions::params::link_target
) inherits guest_additions::params {
  Exec {
    cwd  => $cwd,
    path => [
      '/usr/local/bin',
      '/usr/local/sbin',
      '/usr/bin',
      '/usr/sbin',
      '/bin',
      '/sbin'
    ]
  }

  if $version == undef {
    fail('Guest additions version must be specified.')
  }

  $packages = ['build-essential', 'dkms', 'linux-headers-generic']

  package { $packages:
    ensure  => present
  }

  exec { 'download-guest-additions':
    command   => "wget http://download.virtualbox.org/virtualbox/${version}/VBoxGuestAdditions_${version}.iso",
    creates   => "${cwd}/VBoxGuestAdditions_${version}.iso",
    unless    => "test -d ${install_dir}/VBoxGuestAdditions-${version}/lib/VBoxGuestAdditions",
    timeout   => 1000
  }

  exec { 'create-mount-point':
    command => "mkdir -p ${mount_point}",
    creates => "${mount_point}",
    onlyif  => "test -e ${cwd}/VBoxGuestAdditions_${version}.iso"
  }

  exec { 'mount-guest-additions':
    command     => "mount -o loop,ro VBoxGuestAdditions_${version}.iso ${mount_point}",
    creates     => "${mount_point}/VBoxLinuxAdditions.run",
    refreshonly => true
  }

  exec { 'install-guest-additions':
    command     => "sh ${mount_point}/VBoxLinuxAdditions.run",
    creates     => "${install_dir}/VBoxGuestAdditions-${version}/lib/VBoxGuestAdditions",
    returns     => [0, 1],
    logoutput   => true,
    require     => Package[$packages],
    refreshonly => true
  }

  file { 'path-guest-additions':
    path    => "${link_target}/VBoxGuestAdditions",
    target  => "${install_dir}/VBoxGuestAdditions-${version}/lib/VBoxGuestAdditions",
    ensure  => link,
    force   => true
  }

  Exec['download-guest-additions'] ->
  Exec['create-mount-point'] ~>
  Exec['mount-guest-additions'] ~>
  Exec['install-guest-additions'] ->
  File['path-guest-additions']

  exec { 'unmount-guest-additions':
    command     => "umount ${mount_point}",
    refreshonly => true,
    subscribe   => [
      Exec['mount-guest-additions'],
      Exec['install-guest-additions']
    ]
  }

  exec { 'delete-iso':
    command     => "rm -f ${cwd}/VBoxGuestAdditions_${version}.iso",
    refreshonly => true,
    subscribe   => [
      Exec['download-guest-additions'],
      Exec['unmount-guest-additions']
    ]
  }

  exec { 'delete-mount-point':
    command     => "rm -rf ${mount_point}",
    refreshonly => true,
    subscribe   => [
      Exec['create-mount-point'],
      Exec['unmount-guest-additions']
    ]
  }
}

class mysql::params {
  $package  = 'mysql-server'
  $service  = 'mysql'
  $runas    = 'vagrant'
  $conf_dir = '/etc/mysql'

  $password_column = $facts['os']['name'] ? {
    'Ubuntu' => $facts['os']['release']['major'] ? {
      '18.04' => 'authentication_string',
      default => 'password',
    },
    default => 'password',
  }
}

define mysql::user (
  $ensure   = present,
  $user     = $title,
  $password = undef,
  $database = '*'
) {
  if ! defined(Class['mysql']) {
    fail('Class mysql must be defined.')
  }

  $escapedusername = regsubst($user, '\'', '\\\'')
  $escapedpassword = regsubst($password, '\'', '\\\'')

  if $ensure == 'present' {
    mysql::query { "${user}-user-present":
      query   => template('mysql/create-user.erb'),
      onlyif  => template('mysql/create-user-onlyif.erb')
    }

    mysql::query { "${user}-user-grant":
      query   => template('mysql/user-grant.erb'),
      unless  => template('mysql/user-grant-unless.erb'),
      require => Mysql::Query["${user}-user-present"]
    }

    if $password {
      include mysql::params

      $password_column = $mysql::params::password_column

      mysql::query { "${user}-set-password":
        query   => template('mysql/alter-user-password.erb'),
        onlyif  => template('mysql/set-password-onlyif.erb'),
        require => Mysql::Query["${user}-user-present"]
      }
    }
  } elsif $ensure == 'absent' {
    mysql::query { "${user}-user-absent":
      query => template('mysql/drop-user.erb')
    }
  }
}

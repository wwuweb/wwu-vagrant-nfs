class role::drupalstack {
  include profile::webserver::drupal
  include profile::webserver::includes
  include profile::git
  include profile::nodejs
  include profile::python
  include profile::imagemin
  include profile::sass
  include profile::vboxguest
  include profile::packagemanager
}
